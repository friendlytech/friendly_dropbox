<!DOCTYPE html>
<html>
<head>
<?php
require 'mailgun-php/vendor/autoload.php';
use Mailgun\Mailgun;

if(isset($_GET['email'])){
	$mgClient = new Mailgun('key-2457caa5969c9dadf2dc3a31eed2a733');

	$domain = "mailgun.friendlytechnologiesllc.com";	
	$fromEmail = "Friendly Technologies <friendlycontact@friendlytechnologiesllc.com>";
	 	$result = $mgClient->sendMessage($domain, array(
	    'from'    => $fromEmail,
	    'to'      => "friendlywebhelp@gmail.com",
	    'subject' => "Unsubscribe Request FROM Friendly Tech",
	    'text'    => "UNIX Timestamp: ".time()."\n\nEMAIL TO UNSUBSCRIBE FROM friendlytechnologiesllc.com: ".$_GET['email'] . "\n\n".date("Y-m-j h:i:s")
	));

}
?>
<title>Friendly Technologies, LLC - WEB DESIGN | INTERNET MARKETING | WEB DEVELOPMENT</title>
<?php include('include/snippets/head.php');?>
<style>
	.top_unsubscribe_text{text-align:center;font-size:34px;font-weight:bold;margin-top:30px;}
	.unsubscribe_image{height:226px;width:500px;margin-left:auto;margin-right:auto;}
	.unsubscribe_image img{width:100%;}
	
	

	#free_website {
	    background-color: #cccccc;
	    text-align: center;
	    padding-top: 15px;
	    padding-bottom: 15px;
	    font-size: 40px;
	    font-weight: bold;
	}
	.giveaway_month{text-align:center;font-size:24px;font-weight:bold;}
	#giveaway_info{margin-top: 20px;
	    position: relative;
	    width: 900px;
	    margin-right: auto;
	    margin-left: auto;}
		.left_giveaway{width:50%;float:left;font-size:20px;margin-top: 10px;
	    text-align: justify;
	    padding-left: 10px;
	    line-height: 45px;}
		.left_giveaway p{width:60%;margin-right: auto;margin-left: auto;}
		.right_giveaway{width:50%;float:left;}
		.giveaway_register{font-variant:small-caps;font-size:24px;text-align:center;font-weight:bold;}
		.giveaway_form{background-color: #cccccc;padding:10px;border-radius: 10px 10px 10px 10px;
	    -moz-border-radius: 10px 10px 10px 10px;
	    -webkit-border-radius: 10px 10px 10px 10px;
	width:70%;	margin-right: auto;
	    margin-left: auto;}
	
		#giveaway_thanks{background-color: #cccccc;padding:10px;border-radius: 10px 10px 10px 10px;
	    -moz-border-radius: 10px 10px 10px 10px;
	    -webkit-border-radius: 10px 10px 10px 10px;
	width:70%;	margin-right: auto;
	    margin-left: auto;text-align:center;font-weight:bold;font-size:18px;color: green;
	    text-decoration: underline;}
	
	.giveaway_form label,.giveaway_form .manage_emails_options > label{display:block;	font-weight: 400;
		    font-size: 18px;
	}

	.giveaway_form input[type="radio"] {
	    margin-right: 5px;
	}
	.email_form_section{margin-bottom:0;}
	.manage_emails_options{padding-top:15px;}
	.manage_emails_options :nth-child(n+1){    font-size: 14px;line-height:14px;margin-left: 8px;}
	.manage_emails_options :nth-child(1){  margin-bottom:0;}
	.giveaway_form #submit_btn_homepage{margin-top:10px;}
	
	
	
	#offer_details{width: 750px;margin-top:30px;
    margin-right: auto;
    margin-left: auto;text-align:justify;}
	#offer_details p{border:solid #000 1px;padding:5px;line-height:26px;font-size:16px;}
	#offer_details h4{text-align:center;}

	#giveaway_thanks{display:none;}


	@media only screen and (max-width: 991px) {
		
		#giveaway_info{width:800px;}
		.top_unsubscribe_text{font-size:30px;}
		.left_giveaway{line-height:38px;}
	}
	@media only screen and (max-width: 800px) {
		#free_website{font-size:24px;}
		.unsubscribe_image{height: auto;
	    width: 80%;}
		#giveaway_info{width:100%;padding-left: 10px;margin-top:30px;
	    padding-right: 10px;}
	.left_giveaway{margin-top:50px;line-height:32px;}
	.top_unsubscribe_text{font-size:26px;}
	#offer_details{width: 90%;margin-top:20px;padding-left: 10px;padding-right: 10px;}
	}
	@media only screen and (max-width: 700px) {
		.left_giveaway hr{display:none;}
		#giveaway_info{margin-top:10px;}
		.giveaway_form{width:60%;}
	.left_giveaway,.right_giveaway{margin-top:10px;float:none;width:90%;margin-left:auto;margin-right:auto;}
	.top_unsubscribe_text{font-size:20px;}
	.left_giveaway p{width:100%;}
	
	}
	
	@media only screen and (max-width: 500px) {
		#offer_details p{border:none;}
		.giveaway_form{width:80%;}
		
	}
</style>
<script>
var object = {click:0, validated:0};
	function submit_giveaway_form(){
			
			if(!object.validated){return;}
			
			$(".giveaway_form").slideUp(1000);
			$("#giveaway_thanks").slideDown(1000);
//			$(".submit_btn").html("Thank You").attr('onclick', '').css("cursor","default");
//			$(".thanks_after_submit").delay(950).slideDown(1000);
			
			//SUBMIT EMAIL
			var first_name = $("#first_name_giveaway").val();
			var last_name = $("#last_name_giveaway").val();
			var email = $("#email_giveaway").val();
			var manage_emails_options = $('.manage_emails_options div label input:radio:checked').map(function() {
			    return this.value;
			}).get();

			

			$.post( "send_mail.php", { first_name: first_name, last_name: last_name, email: email, manage_emails_options: manage_emails_options.join(" ") } );
			
		
	}
	
	function validate_giveaway_form(){
		if(object.click==0){return;}
		object.validated = 1;
		$(".giveaway_form > div > label > input").each( function (i, elem) {
			if($("input[name='"+elem.name+"']").val() == ""){
				$("input[name='"+elem.name+"']").css("border","solid red 2px");
				object.validated = 0;				
			}
			else{
				$("input[name='"+elem.name+"']").css("border","none");
			}
		});
		
		$(".manage_emails_options > div > label > input").each( function (i, elem) {
			if($("input[name='"+elem.name+"']").val() == ""){
				$("input[name='"+elem.name+"']").css("border","solid red 2px");
				object.validated = 0;
			}
			else{
				$("input[name='"+elem.name+"']").css("border","none");
			}
		});
		if($("input[name='manage_email']:checked").val() != undefined){			
			$(".manage_emails_options").css("border","none");
		}
		else{
			$(".manage_emails_options").css("border","solid red 2px");
			object.validated = 0;
		}
		if($(".email_form_section label input").val() != ""){
			if(!isValidEmailAddress($(".email_form_section label input").val())){
				$(".email_form_section label input").css("border","solid red 2px");
				object.validated = 0;
			}
		}
		
		
		//
		/*
		$( 'input.contact_info' ).each( function (i, elem) {
			if($("input[name='"+elem.name+"']").val() == ""){
				$("input[name='"+elem.name+"']").css("border","solid red 2px");
			}
			else{
				$("input[name='"+elem.name+"']").css("border","none");
			}
		} );*/
	}
	
	function isValidEmailAddress(emailAddress) {
	    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	    return pattern.test(emailAddress);
	};
</script>
</head>
<body>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-61660395-2', 'auto');
	  ga('send', 'pageview');

	</script>
	<div id="main_container">
		<?php include('include/snippets/header_top_bar.php');?>
		<section id="top_unsubscribe">
			<div class="top_unsubscribe_text">We already miss you.</div>
			<div class="unsubscribe_image"><img src="images/AdobeStock_93672414.jpeg">
		</section>
		<section id="free_website">Register to win a $2500 Website</section>
		<img id="orangetriangle" src="images/orangetriangle.png">
		<div class="giveaway_month">Quarterly giveaway!!!</div>
		<section id="giveaway_info">
			<div class="left_giveaway"><p>If you, or anybody you know has a need for a company website, they can register to <b>win a $2,500 website.</b></p><hr /><p>The winner will be announced on <b>Sunday, June the 4th.</b></p></div>
			<div class="right_giveaway">
				<div class="giveaway_register">Yes, I want to register</div>
				<form class="giveaway_form">
					<div class="form-group">
						<label class="control-label">First Name<input id="first_name_giveaway" type="text" name="first_name" class="form-control" placeholder="John" onchange="validate_giveaway_form();" required></label>
					</div>
					<div class="form-group">
						<label class="control-label">Last Name<input id="last_name_giveaway" type="text" name="last_name" class="form-control" placeholder="Doe" onchange="validate_giveaway_form();" required></label>
					</div>
					<div class="form-group email_form_section">
						<label  class="control-label">Email<input id="email_giveaway" type="email" name="email" class="form-control" placeholder="email@example.com" onchange="validate_giveaway_form();" required></label>
					</div>
					<div class="manage_emails_options">
						<label>Manage Emails</label>
						<div class="form-group">
							<label class="control-label"><input type="radio" name="manage_email" value="subscribe" onchange="validate_giveaway_form();" required>Send me future offers</label>
							<label class="control-label"><input type="radio" name="manage_email" value="giveaway_only" onchange="validate_giveaway_form();" required>Giveaway notice only</label>
						</div>
					</div>
					<button id="submit_btn_homepage" type="button" class="btn btn-warning" onclick="object.click=1;validate_giveaway_form();submit_giveaway_form();">Register to Win!</button>
				</form>		
				<div id="giveaway_thanks">Thank you for your submission!</div>
			</div>
			<div style="clear:both;"></div>
			
		</section>
		<section id="offer_details">
			<h4>Offer Details</h4>
			<p>Simply share a business case with us about why your company needs a new website. The winner will receive a website worth up to $2,500, tailored to your unique business. Website features include: 5 pages, social media integration, informational company video, website direct messaging, eCommerce, secure payment portal, SEO, Google Analytics, and more!</p>
		</section>
		<footer>Friendly Technologies - 5516 Joe Sayers Ave. Austin, TX 78756 - 512.903.2989 - friendlywebhelp@gmail.com<div class="social_icons">
			<a target="_blank" href="https://plus.google.com/u/0/111176302629462345219"><img src="images/social_icons/1458629411_google_circle_color.png"></a>
			<a target="_blank" href="https://www.youtube.com/channel/UCNovmboZZIQE_i2mxsJMwDw"><img src="images/social_icons/1458629448_youtube_circle_color.png"></a>
			<!--<a href=""><img src="images/social_icons/1458629292_facebook_circle_color.png"></a>
			<a href=""><img src="images/social_icons/1458629549_linkedin_circle_color.png"></a>-->
		</div>
		</footer>
	</div>
</body>
</html>