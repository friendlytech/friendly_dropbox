<!DOCTYPE html>
<html>
<head>
<title>Friendly Technologies, LLC</title>
<?php chdir("../../"); $page_type = "payment_gateway"; include('include/snippets/head.php');?>
</head>
<body>
	<div id="main_container">
		<?php include('include/snippets/header_top_bar.php');?>
		<section class="payment_gateway">
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="NECWMFG382WVE">
			<table>
			<tr><td><input type="hidden" name="on0" value="O&M Solutions Monthly Hosting"><span class="payment_client">O&amp;M Solutions Monthly Hosting Subscription</span> </td></tr><tr><td><input type="hidden" name="os0" maxlength="200" class="form-control"  value="Includes 1.5 hours development work available for use each month." ></td></tr>
			</table><br />
			<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</section>


		<footer>Friendly Technologies - 5516 Joe Sayers Ave. Austin, TX 78756 - 512.903.2989 - friendlywebhelp@gmail.com<div class="social_icons">
			<a target="_blank" href="https://plus.google.com/u/0/111176302629462345219"><img src="<?=$dir_prefix?>images/social_icons/1458629411_google_circle_color.png"></a>
			<a target="_blank" href="https://www.youtube.com/channel/UCNovmboZZIQE_i2mxsJMwDw"><img src="<?=$dir_prefix?>images/social_icons/1458629448_youtube_circle_color.png"></a>
			<!--<a href=""><img src="<?=$dir_prefix?>images/social_icons/1458629292_facebook_circle_color.png"></a>
			<a href=""><img src="<?=$dir_prefix?>images/social_icons/1458629549_linkedin_circle_color.png"></a>-->
		</div>
		</footer>
	</div>
</body>
</html>