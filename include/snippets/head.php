<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />

<?php if( isset($page_type) && $page_type == "payment_gateway") $dir_prefix = "../../"; else $dir_prefix = ""; ?>
<link rel="stylesheet" href="<?=$dir_prefix?>include/css/style.css" type="text/css" charset="utf-8" />
<script src="<?=$dir_prefix?>include/bootstrap/js/jquery-1.10.2.js" type="text/javascript" charset="utf-8"></script>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>

<!--BOOTSTRAP-->
<script src="<?=$dir_prefix?>include/bootstrap/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?=$dir_prefix?>include/bootstrap/css/bootstrap.css" type="text/css" charset="utf-8" />

<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">



<!--DATEPICKER-->
<script src="<?=$dir_prefix?>include/datepicker/moment.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?=$dir_prefix?>include/datepicker/bootstrap-datetimepicker.js"></script>
<link rel="stylesheet" href="<?=$dir_prefix?>include/datepicker/bootstrap-datetimepicker.css" />

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({format: 'M/D/YYYY'});
        $('#datetimepicker2').datetimepicker({format: 'M/D/YYYY'});
    });
</script>

<script src="<?=$dir_prefix?>include/js/validator.min.js"></script>

<script>

$(function () {
	$(".booking_form_homepage_small_btn,.booking_form_homepage_large_btn").click(function(){
		if ( !$( this ).hasClass( "active" ) ) {$(this).addClass( "active" );}
		else{$(this).removeClass( "active" );}
	});
		
});



$(function () {

    /* SET PARAMETERS */
    var change_img_time     = 6000; 
    var transition_speed    = 2000;

    var slideshow_container    = $("#slider_main"),
        listItems           = slideshow_container.children('img'),
        listLen             = listItems.length,
        i                   = 0,
		slider_links    = $("#slider_links"),
		slider_link_items  = slider_links.children('div'),
		offset_amt = 1000,

        changeList = function () {
	
		if($(window).width()<1000 && $(window).width()>=768){offset_amt = 748;}
		else if($(window).width()<768 && $(window).width()>500){offset_amt = 480;}
		else{offset_amt = 1000;}//changes offset based on window width
            listItems.eq(i).css("z-index",3).animate({left: -offset_amt},transition_speed,function(){
				$(this).css("z-index",1);//put image on bottom of stack
	});//animate current image out
			slider_link_items.eq(i).removeClass("active"); 
			i += 1;//next slide
			if (i === listLen) {i = 0;//start over
                }
				slider_link_items.eq(i).addClass("active"); 
				listItems.eq(i).css({"z-index":2,"left":offset_amt}).animate({left: "0"},transition_speed);//animate next image in
				
				
				
			};
			
			
	listItems.css({"z-index":3,"left":0});//prepare
    listItems.not(':first').css({"z-index":1,"left":1000});//prepare
    setInterval(changeList, change_img_time);//run slideshow

});

function shake_badmessage(id){
	$("#"+id).animate({left: "10px"},100,
		function(){$("#"+id).animate({left: "0px"},100,
			function(){$("#"+id).css("left","initial").animate({right: "10px"},100,
				function(){$("#"+id).animate({right: "0px"},100,
					function(){$("#"+id).css("right","initial");}
				)}
			)}
		)}
	);
	
}
</script>


<script>
$(function () {
var mybutton = document.getElementById('submit_button');
//console.log(mybutton);
mybutton.onclick = function() {
	var request;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else {
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var name = $('#send_message_name').val();
	var email = $('#send_message_email').val();
	var message = $('#send_message_message').val();
	request.open('GET', 'submit_message.php?name=>'+name +'&email=>'+email +'&message=>'+message);
//	console.log(request);
	request.onreadystatechange = function() {
		if ((request.readyState===4) && (request.status===200)) {
//			var items = JSON.parse(request.responseText);
//			var output = '<ul>';
//			for (var key in items) {
//				output += '<li>' + items[key].name + '</li>';
//			}
//			output += '</ul>';
			//document.getElementById('update').innerHTML = output;
			if(request.responseText == "1") {console.log("success");$("#send_message_form").slideUp(1000);$("#send_message_confirm").slideDown(1000);}//SUCCESS
			else {console.log("fail");$("#send_message_fail").slideDown(1000);//FAIL
			shake_badmessage('send_message_fail');}
			console.log(request.responseText);
			
		}
	}
	request.send();
} // loadAJAX
})

$(function () {
var mybutton = document.getElementById('submit_btn_homepage');
//console.log(mybutton);
mybutton.onclick = function() {
	var request;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else {
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
//	
//	var email = $('#send_message_email').val();
//	var message = $('#send_message_message').val();
	var services = "";
	$( 'input[name^="service"]:checked' ).each( function() {services +=  this.value +", ";});
	var your_site = $('#your_site_form_section_input').val();
	var competitor_site = $('#competitor_site_form_section_input').val();
	var start_date = $('#start_date').val();
	var end_date = $('#complete_date').val();
	var form_part = $('#form_part').val();
	
	var need = "";
	$( 'input[name^="need"]:checked' ).each( function() {need +=  this.value +", ";});

	var your_name = $('#your_name_form_section_input').val();
//	if(your_name =="undefined") $('#your_name_form_section_input').val("");
	var your_email = $('#your_email_form_section_input').val();
//	if(your_email =="undefined") $('#your_email_form_section_input').val("");
	
	var your_comments = $('#your_comments_form_section_input').val();
	
	if (form_part == 1)			
	{			
		request.open('GET', 'submit_message.php?services='+services+'&need='+need+
		'&your_site='+your_site+
		'&competitor_site='+competitor_site+
		'&start_date='+start_date+
		'&end_date='+end_date+
		'&form_part='+form_part);
	//	console.log(request);
		request.onreadystatechange = function() {
			if ((request.readyState===4) && (request.status===200)) {
				if(request.responseText == "1") {//SUCCESS
					console.log("success");
					$("#main_form_fail,#out_of_town_form_section,#your_site_form_section,#competitor_site_form_section,#for_these_dates_form_section,#do_you_need_form_section").slideUp(1000);
					$("#your_name_form_section,#your_email_form_section,#your_comments_form_section").slideDown(500);
					$("#submit_btn_homepage").css("margin-top",0);
					$("#submit_btn_form_section").animate({width: "100%"},1500);
//					$("#booking_form_homepage").animate({width: "480px",height:"230px"},1500);
					$("#booking_form_homepage").addClass('second_page_booking_form');
					$('#form_part').val(2);
				}
				else {console.log("fail");$("#main_form_fail").slideDown(1000);//FAIL
				shake_badmessage('main_form_fail');}
				console.log(request.responseText);
			
			}
		}
	
	}
	else if (form_part == 2)			
	{			
		request.open('GET', 'submit_message.php?services='+services+'&need='+need+
		'&your_site='+your_site+
		'&competitor_site='+competitor_site+
		'&start_date='+start_date+
		'&end_date='+end_date+
		'&form_part='+form_part+
		'&your_name='+your_name+
		'&your_email='+your_email+
		'&your_comments='+your_comments);
	//	console.log(request);
		request.onreadystatechange = function() {
			if ((request.readyState===4) && (request.status===200)) {
				if(request.responseText == "1") {//SUCCESS
					console.log("success");
					//$("#out_of_town_form_section,#your_site_form_section,#competitor_site_form_section,#for_these_dates_form_section,#do_you_need_form_section").slideUp(1000);
					$("#main_form_fail,#your_name_form_section,#your_email_form_section,#your_comments_form_section").slideUp(1000);
					$("#submit_btn_homepage").fadeOut(1000);
					$("#main_form_success").fadeIn(1000);
					$("#booking_form_homepage").animate({height:"90px"},1500);
					
					
					//$("#submit_btn_homepage").css("margin-top",0);
					//$("#submit_btn_form_section").css("width","100%");
					//$("#booking_form_homepage").animate({width: "480px",height:"230px"},1500);
					//$('#form_part').val(2);
				}
				else {console.log("fail");$("#main_form_fail").slideDown(1000);//FAIL
				shake_badmessage('main_form_fail');}
				console.log(request.responseText);
			
			}
		}
	
	}
	request.send();
} // loadAJAX
})

//
</script>
<meta name="description" content="Websites that highlight your business as the expert in your LOCAL market - 512.903.2989 - friendlywebhelp@gmail.com" />