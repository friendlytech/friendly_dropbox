<!DOCTYPE html>
<html>
<head>
<title>Friendly Technologies, LLC - WEB DESIGN | INTERNET MARKETING | WEB DEVELOPMENT</title>
<?php include('include/snippets/head.php');?>
</head>
<body>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-61660395-2', 'auto');
	  ga('send', 'pageview');

	</script>
	<div id="main_container">
		<?php include('include/snippets/header_top_bar.php');?>
		<section id="top_slider">
			<div id="slider_tagline"><b>WEBSITES</b> that highlight your <b>BUSINESS</b> as the expert in your <b>LOCAL MARKET</b>.  Make it easy for 
			<b>WEB VISITORS</b> to find the information they need to purchase your <b>SERVICES</b> or <b>PRODUCTS</b>.</div>
			<div id="slider_links"><div class="active">WEB DESIGN</div> | <div>INTERNET MARKETING</div> | <div>WEB DEVELOPMENT</div></div>
			
			<!--
			<div id="slider_main">
				<img class="slide_image" src="images/slider_1.png">
				<img class="slide_image" src="images/slider_2.png">
				<img class="slide_image" src="images/slider_3.png">
			</div>-->
		</section>
		<section class="video">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/nnebwYWjq5Q?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
		</section>
		<section id="free_consult">Schedule a FREE consultation Today!</section>
		<img id="orangetriangle" src="images/orangetriangle.png">
		<section>
			<form id="booking_form_homepage" action="booking.php" method="post">
				<div id="booking_form_homepage_services">
					<div id="out_of_town_form_section">
						<label>
							<input type="checkbox" name="service" value="Website Design" />
							<div class="booking_form_homepage_large_btn">
								<img src="images/form_icons/webdesignicon.png">
								<div class="booking_form_homepage_large_btn_text">Website Design</div>
							</div>
						</label>
						<label>
							<input type="checkbox" name="service" value="eCommerce" />
							<div class="booking_form_homepage_large_btn">
								<img src="images/form_icons/ecommerceicon.png">
								<div class="booking_form_homepage_large_btn_text">eCommerce</div>
							</div>
						</label>
						<label>
							<input type="checkbox" name="service" value="Software Dev" />
							<div class="booking_form_homepage_large_btn">
								<img src="images/form_icons/softwaredevicon.png">
								<div class="booking_form_homepage_large_btn_text">Software Dev</div>
							</div>
						</label>
						<label>
							<input type="checkbox" name="service" value="SEO" />
							<div class="booking_form_homepage_large_btn">
								<img src="images/form_icons/seoicon.png">
								<div class="booking_form_homepage_large_btn_text">SEO</div>
							</div>
						</label>
						<label>
							<input type="checkbox" name="service" value="PPC" />
							<div class="booking_form_homepage_large_btn">
								<img src="images/form_icons/ppcicon.png">
								<div class="booking_form_homepage_large_btn_text">PPC</div>
							</div>
						</label>
					</div>
				</div>
				<div id="your_site_form_section"><div class="form_section_title">Your Site</div>
					<input type="text" name="your_site" class="form-control" id="your_site_form_section_input" placeholder="www.mywebsite.com">
				</div>
				<div id="competitor_site_form_section"><div class="form_section_title">Competitor Site</div>
					<input type="text" name="competitor_site" class="form-control" id="competitor_site_form_section_input" placeholder="www.competitorsite.com">
				</div>
				<div id="for_these_dates_form_section"><div class="form_section_title">Is there a deadline?</div>
					<div class='input-group date col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6' id='datetimepicker1'>
			            <input name="datetimepicker1" type='text' class="form-control" id="start_date" placeholder="Start Date" />
			            <span class="input-group-addon">
			                <span class="glyphicon glyphicon-calendar"></span>
			            </span>
			        </div>
					<div class='input-group date col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6' id='datetimepicker2'>
			            <input name="datetimepicker2" type='text' class="form-control" id="complete_date" placeholder="Complete Date" />
			            <span class="input-group-addon">
			                <span class="glyphicon glyphicon-calendar"></span>
			            </span>
			        </div>
				</div>
				<div id="do_you_need_form_section"><div class="form_section_title">Do you Need</div>
					<label>
						<input type="checkbox" name="need" value="Content" />
						<div class="booking_form_homepage_small_btn">
							<div class="booking_form_homepage_small_btn_text">Content</div>
						</div>
					</label>
					<label>
						<input type="checkbox" name="need" value="Graphic Design" />
						<div class="booking_form_homepage_small_btn">
							<div class="booking_form_homepage_small_btn_text booking_form_homepage_small_btn_graphic_design">Graphic Design</div>
						</div>
					</label>
					<label>
						<input type="checkbox" name="need" value="Monthly Support" />
						<div class="booking_form_homepage_small_btn">
							<div class="booking_form_homepage_small_btn_text booking_form_homepage_small_btn_monthly_support">Monthly Support</div>
						</div>
					</label>
					<label>
						<input type="checkbox" name="need" value="Hosting" />
						<div class="booking_form_homepage_small_btn">
							<div class="booking_form_homepage_small_btn_text">Hosting</div>
						</div>
					</label>
				</div>
				<input type="hidden" name="form_part" id="form_part" value="1">
				<div id="your_name_form_section"><div class="form_section_title">Your Name</div>
					<input type="text" name="your_name" class="form-control" id="your_name_form_section_input" placeholder="What is your name?">
				</div>
				<div id="your_email_form_section"><div class="form_section_title">Your Email</div>
					<input type="text" name="your_email" class="form-control" id="your_email_form_section_input" placeholder="me@example.com">
				</div>
				<div id="your_comments_form_section"><div class="form_section_title">Comments</div>
					<textarea type="text" name="your_comments" class="form-control" id="your_comments_form_section_input" placeholder="Anything else?"></textarea>
				</div>
				<div id="submit_btn_form_section"><br />
					<div id="main_form_success">Thank you! Your message has been sent</div>
					<button id="submit_btn_homepage" type="button" class="btn btn-warning">Schedule a FREE Consultation</button>					
					<div id="main_form_fail"><br />Please check to make sure you filled out all form fields correctly</div>
					
				</div>
				&nbsp;<br />
			</form>
		</section>
		<section id="homepage_services">
			<div class="orange_line"></div>
			<div class="orange_line_title">Services</div>
			<div class="homepage_service row">
				<div class="col-md-6 col-lg-6">
					<div class="homepage_service_image"><img src="images/services/Services1.png"></div>
				</div>
				<div class="col-md-6 col-lg-6">
					<div class="homepage_service_section_title">Website Design</div>
					<div class="homepage_service_description">We create meaningful websites that offer simplistic user experiences that rank high for the most popular search terms for your business.</div>
				</div>
			</div>
			<div class="homepage_service row">
				<div class="col-md-6 col-lg-6 hidden-md hidden-lg">
					<div class="homepage_service_image"><img src="images/services/Services2.png"></div>
				</div>
				<div class="col-md-6 col-lg-6">
					<div class="homepage_service_section_title">Custom Development</div>
					<div class="homepage_service_description">Custom applications developed using PHP, MySQL, Java, JavaScript, Python and C++. Code is clean, stable and focuses on the user experience.</div>
				</div>
				<div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
					<div class="homepage_service_image"><img src="images/services/Services2.png"></div>
				</div>
			</div>
			<div class="homepage_service row">
				<div class="col-md-6 col-lg-6"><div class="homepage_service_image"><img src="images/services/Services3.png"></div></div>
				<div class="col-md-6 col-lg-6">
					<div class="homepage_service_section_title">Internet Marketing</div>
					<div class="homepage_service_description">SEO, PPC &amp; local Search-ability make it easy for web users and search engines to find your website. Google owned platforms are increasingly important.</div>
				</div>
			</div>
			
		</section>
		<section id="homepage_portfolio">
			<div class="orange_line"></div>
			<div class="orange_line_title">Portfolio</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<a href="http://www.thomaslondonbooks.com" class="homepage_portfolio" target="_blank">
							<div class="portfolio_image"><img src="images/portfolio/portfolio1.png"></div>
							<div class="portfolio_title">Thomas London Books</div>
					</a>
				</div>
				<div class="col-md-6 col-lg-6">
					<a href="http://www.kelvinbatiste.com"  class="homepage_portfolio" target="_blank">
						<div class="portfolio_image"><img src="images/portfolio/PortfolioImage2-01.png"></div>
						<div class="portfolio_title">Kelvin Batiste - Creative Director</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<a href="http://www.nextgenwebsitedesign.com"  class="homepage_portfolio" target="_blank">
							<div class="portfolio_image"><img src="images/portfolio/PortfolioImage3-01.png"></div>
							<div class="portfolio_title">NextGen Website Design</div>
					</a>
				</div>
				<div class="col-md-6 col-lg-6">
					<a href="http://www.rtccons.com"  class="homepage_portfolio" target="_blank">
						<div class="portfolio_image"><img src="images/portfolio/PortfolioImage4-01.png"></div>
						<div class="portfolio_title">RTC Construction Management</div>
					</a>
				</div>
			</div>
		</section>
		<section id="homepage_contact_us">
			<div class="orange_line"></div>
			<div class="orange_line_title">Contact Us</div>
			<img id="contact_image" src="images/contact.png">
			<div class="homepage_contact_us_phone_email"><a href="tel:5129032989">(512) 903-2989</a></div>
			<div class="homepage_contact_us_phone_email"><a href="mailto:friendlywebhelp@gmail.com">friendlywebhelp@gmail.com</a></div>
			<form id="send_message_form">
				<input type="text" id="send_message_name" class="form-control" placeholder="Name">
				<input type="email" id="send_message_email"  class="form-control" placeholder="Email">
				<textarea rows="4"  id="send_message_message"  class="form-control" placeholder="Message"></textarea>
				<div id="send_message_fail">Please check to make sure you filled out all form fields correctly</div>
				<button type="button" id="submit_button" class="btn btn-primary">SEND MESSAGE</button>
			</form>
			<div id="send_message_confirm">Message Sent - Thank you!</div>
		</section>
		<footer>Friendly Technologies - 5516 Joe Sayers Ave. Austin, TX 78756 - 512.903.2989 - friendlywebhelp@gmail.com<div class="social_icons">
			<a target="_blank" href="https://plus.google.com/u/0/111176302629462345219"><img src="images/social_icons/1458629411_google_circle_color.png"></a>
			<a target="_blank" href="https://www.youtube.com/channel/UCNovmboZZIQE_i2mxsJMwDw"><img src="images/social_icons/1458629448_youtube_circle_color.png"></a>
			<!--<a href=""><img src="images/social_icons/1458629292_facebook_circle_color.png"></a>
			<a href=""><img src="images/social_icons/1458629549_linkedin_circle_color.png"></a>-->
		</div>
		</footer>
	</div>
</body>
</html>